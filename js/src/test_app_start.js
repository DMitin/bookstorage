var bookStoreApp = new Backbone.Marionette.Application();
/*
bookStoreApp.addInitializer(function(options){
    console.log("initializer");
});
*/
bookStoreApp.on('start', function () {
    if (Backbone.history) {
        Backbone.history.start();
    }
    console.log('start');
});

bookStoreApp.start();