App.module('BookGallery', function (BookGallery, App) {

    BookGallery.BookView = Marionette.ItemView.extend({
        template: "#book-view-template"
    });

    var CategoryView = Backbone.Marionette.ItemView.extend({
        tagName : 'li',
        template: "#book-view-template"
    });


    BookGallery.BookColletionView = Backbone.Marionette.CollectionView.extend({
        class: "row",
        childView: BookGallery.BookView
    });


    BookGallery.addInitializer(function(args) {
        console.log("gallery view initialized");
        BookGallery.controller.showGallery();
    });


});
