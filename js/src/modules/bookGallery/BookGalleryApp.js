App.module('BookGallery', function (BookGallery, App) {

    var MyRouter = Backbone.Router.extend({
        routes: {
            "": "gallery"
        },

        gallery: function(bookId) {
            BookGallery.controller.showGallery()
        }
    });

    var Controller = Marionette.Controller.extend({
        showGallery: function() {
            var collection = new Backbone.Collection([
                {}, {}
            ]);
            var bookCollectionView = new BookGallery.BookColletionView({
                collection: collection
            });
            App.containerRegion.show(bookCollectionView);
        }
    })

    BookGallery.addInitializer(function(args) {
        console.log("gallery router");
        BookGallery.controller = new Controller({});
        var router = new MyRouter();
    });

});