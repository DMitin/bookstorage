App.module('BookDetails', function (BookDetails, App) {

    var MyRouter = Backbone.Router.extend({
        routes: {
            "book/:query": "book"
        },

        book: function(bookId) {
            console.log("router->" + bookId);
        }
    });

    var Controller = Marionette.Controller.extend({
        showBook: function(bookId) {
            console.log(bookId);

            var SampleModel = Backbone.Model.extend({
                defaults : {
                    value1 : "A random Value",
                    value2 : "Another Random Value"
                }
            });

            var sampleModel = new SampleModel();
            var sampleView = new BookInfo({model:sampleModel});
            App.containerRegion.show(sampleView);



        }
    });

    BookDetails.addInitializer(function(args) {
        console.log("hello");
        var router = new MyRouter();
        BookDetails.controller = new Controller({});

        router.on("route:book", function(bookId) {
            BookDetails.controller.showBook(bookId);
        });


    });

});
