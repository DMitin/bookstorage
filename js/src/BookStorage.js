var BookStorage = {};
var App = new Marionette.Application();

App.addRegions({
    containerRegion: '#container'
});

App.on('start', function () {
    if (Backbone.history) {
        Backbone.history.start();
    }
});

App.startSubApp = function (appName, args) {
    var currentApp = App.module(appName);
    if (App.currentApp === currentApp) { return; }

    if (App.currentApp) {
        App.currentApp.stop();
    }

    App.currentApp = currentApp;
    currentApp.start(args);
};

App.start();


//App.startSubApp("BookDetails");


/*
var MyRouter = Backbone.Router.extend({
    routes: {
        "details": "details"
    },

    details: function (category,id) {
        console.log("show details");
    }
});
*/
/*
Backbone.history.start();

var AppRouter = Backbone.Router.extend({
    routes: {
        "details": "defaultRoute" // matches http://example.com/#anything-here
    }
});
// Initiate the router
var app_router = new AppRouter;

app_router.on('route:defaultRoute', function(actions) {
    alert(actions);
});
*/
//App.start();

// Start Backbone history a necessary step for bookmarkable URL's
//Backbone.history.start();
